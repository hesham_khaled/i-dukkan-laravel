<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\parent_category;
use App\category;
use App\ads;

class Pagecontroll extends Controller
{
    public function index (){

    	return 'home page';
    }

    public function showMainCat() {
    	$parent_category = parent_category::all();//['parent_cat_id', 'pcat_name_en']
    	return $parent_category;

    }

    public function showSubCat ($id){
	$category = category::getcatsub($id)->get();//['cat_id', 'cat_name_en']
	return $category; 
    }


     public function showAllAds() {
        $ads = ads::all();//['ads_id', 'title','img']
        return $ads;

    }

}
